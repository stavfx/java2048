package presenter;

import http.HelpRequest;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import model.GameState;
import model.IModel;
import model.ModelState;
import view.IView;
import view.ViewEvent;
import data.Direction;

public class Presenter implements Observer {
	private IModel model;
	private IView view;
	private ExecutorService modelThreadPool;
	private ExecutorService httpThreadPool;

	public Presenter(IModel model, IView view) {
		this.model = model;
		this.view = view;
		modelThreadPool = Executors.newSingleThreadExecutor();
		httpThreadPool = Executors.newSingleThreadExecutor();
		model.addObserver(this);
		view.addObserver(this);

		GameState state = model.getGameState();
		boolean won = state == GameState.WON;
		boolean over = state == GameState.OVER;
		view.displayData(model.getData(), model.getScore(), won, over);
	}

	public void registerInputSender(Observable o) {
		o.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof ViewEvent) {
			ViewEvent event = (ViewEvent) arg;
			handleInputEvent(event);
		} else if (arg instanceof HelpRequest) {
			HelpRequest request = (HelpRequest) arg;
			handleHelpRequested(request);
		} else if (arg instanceof ModelState) {
			ModelState state = (ModelState) arg;
			boolean won = state.getState() == GameState.WON;
			boolean over = state.getState() == GameState.OVER;
			view.displayData(state.getData(), state.getScore(), won, over);
		}
	}

	private void handleInputEvent(final ViewEvent event) {
		modelThreadPool.execute(new Runnable() {

			@Override
			public void run() {
				switch (event) {
					case DOWN:
						model.moveDown();
						break;
					case LEFT:
						model.moveLeft();
						break;
					case RIGHT:
						model.moveRight();
						break;
					case UP:
						model.moveUp();
						break;
					case DOWN_LEFT:
						model.moveDownLeft();
						break;
					case DOWN_RIGHT:
						model.moveDownRight();
						break;
					case UP_LEFT:
						model.moveUpLeft();
						break;
					case UP_RIGHT:
						model.moveUpRight();
						break;

					case VIEW_READY:
						GameState state = model.getGameState();
						boolean won = state == GameState.WON;
						boolean over = state == GameState.OVER;
						view.displayData(model.getData(), model.getScore(), won, over);
						break;
					case UNDO:
						model.undo();
						break;
					case RESET:
						model.reset();
						break;

					case SAVE:
						model.saveToFile(event.getArgs());
						break;

					case LOAD:
						model.loadFromFile(event.getArgs());
						break;

				}
			}
		});
	}

	private void handleHelpRequested(final HelpRequest request) {
		httpThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < request.getHints(); i++) {
					request.setState(model.getData(), model.getScore());
					Direction direction = request.execute();
					if (direction == Direction.NONE)
						return;
					handleHelpReceieved(direction);
				}
			}
		});
	}

	private void handleHelpReceieved(final Direction direction) {
		switch (direction) {
			case DOWN:
				model.moveDown();
				break;
			case LEFT:
				model.moveLeft();
				break;
			case RIGHT:
				model.moveRight();
				break;
			case UP:
				model.moveUp();
				break;
			default:
				break;
		}
	}

}
