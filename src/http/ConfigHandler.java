package http;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeSet;

import com.google.gson.Gson;

public class ConfigHandler {
	private static final String DEFAULT_SERVER = "127.0.0.1";

	public static class ServerConfig {
		private String selectedServer;
		private TreeSet<String> serverList = new TreeSet<String>();

		public String getSelectedServer() {
			return selectedServer;
		}

		public String[] toArray() {
			return serverList.toArray(new String[serverList.size()]);
		}
	}

	public static ServerConfig getServerConfig() {
		File file = new File("server.cfg");
		ServerConfig config = null;
		if (!file.exists()) {
			try {
				file.createNewFile();
				config = new ServerConfig();
				config.selectedServer = DEFAULT_SERVER;
				config.serverList.add(DEFAULT_SERVER);
				saveServerConfig(config);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			config = readFromFile(file, ServerConfig.class);
		}
		return config;
	}

	private static void saveServerConfig(ServerConfig config) {
		File file = new File("server.cfg");
		writeToFile(file, new Gson().toJson(config));
	}

	public static String getCurrentServer() {
		return getServerConfig().getSelectedServer();
	}

	public static void setCurrentServer(String address) {
		ServerConfig config = getServerConfig();
		config.selectedServer = address;
		config.serverList.add(address);
		saveServerConfig(config);
	}

	private static void writeToFile(File file, String content) {
		try {
			PrintWriter pw = new PrintWriter(file);
			pw.write(content);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static <T> T readFromFile(File file, Class<T> klass) {
		try {
			String content = "";
			String line;
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null)
				content += line;
			br.close();
			return new Gson().fromJson(content, klass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
