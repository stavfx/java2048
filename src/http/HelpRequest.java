package http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

import data.Direction;
import data.Game2048HelpRequest;

public class HelpRequest {
	private Game2048HelpRequest request;
	private String server;
	private int hints;

	public HelpRequest(String server, int hints) {
		this.server = server;
		this.hints = hints;
	}

	public void setState(int[][] state, int score) {
		request = new Game2048HelpRequest(state, score);
	}

	public int getHints() {
		return hints;
	}

	public Direction execute() {
		Gson gson = new Gson();
		try {
			URL url = new URL(server);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(gson.toJson(request));
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = conn.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();

			return gson.fromJson(response.toString(), Direction.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Direction.NONE;
	}
}
