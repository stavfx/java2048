package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public abstract class Board extends Composite {
	private BaseTile[][] tiles;

	public Board(Composite parent, int style) {
		super(parent, style);
	}

	public void setData(int[][] data) {
		if (tiles == null) {
			tiles = new BaseTile[data.length][data[0].length];
			setLayout(new GridLayout(tiles[0].length, true));
			for (int i = 0; i < tiles.length; i++)
				for (int j = 0; j < tiles[i].length; j++) {
					tiles[i][j] = makeNewTile();
					tiles[i][j].setEnabled(false);
					tiles[i][j].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				}

			layout();
		}

		for (int i = 0; i < tiles.length; i++)
			for (int j = 0; j < tiles[i].length; j++) {
				tiles[i][j].setValue(data[i][j]);
			}
	}

	protected abstract BaseTile makeNewTile();

}
