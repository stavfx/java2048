package view;

public enum ViewEvent {
	UP, DOWN, LEFT, RIGHT,
	UP_RIGHT, UP_LEFT, DOWN_RIGHT, DOWN_LEFT,
	VIEW_READY,
	UNDO, RESET,
	SAVE, LOAD;

	private String args;

	public String getArgs() {
		return args;
	}

	public ViewEvent setArgs(String args) {
		this.args = args;
		return this;
	}

	public ViewEvent join(ViewEvent other) {
		switch (this) {
		case UP:
			if (other == LEFT)
				return UP_LEFT;
			if (other == RIGHT)
				return UP_RIGHT;
			break;
		case DOWN:
			if (other == LEFT)
				return DOWN_LEFT;
			if (other == RIGHT)
				return DOWN_RIGHT;
			break;
		case LEFT:
			if (other == UP)
				return UP_LEFT;
			if (other == DOWN)
				return DOWN_LEFT;
			break;
		case RIGHT:
			if (other == UP)
				return UP_RIGHT;
			if (other == DOWN)
				return DOWN_RIGHT;
			break;
		}

		return null;
	}
}
