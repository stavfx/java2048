package view;

import game_2048.model.Game2048Model;
import game_2048.view.Game2048View;
import maze.model.MazeModel;
import maze.view.MazeView;
import model.IModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import presenter.Presenter;

public class GameSelector {
	private Display display;
	private Shell shell;

	public GameSelector(Display display) {
		this.display = display;

		shell = new Shell(display);
		shell.setSize(300, 400);
		shell.setLayout(new GridLayout(1, false));
		shell.setText("Let's Play A Game!");

		makeButton("2048", launch2048());
		makeButton("Maze", launchMaze());
	}

	public void open() {
		shell.open();
	}

	private void makeButton(String text, SelectionListener listener) {
		Button button = new Button(shell, SWT.PUSH);
		button.setText(text);
		button.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		setFont(button);
		button.addSelectionListener(listener);
	}

	private void setFont(Button button) {
		Font f = button.getFont();
		Font nf = new Font(display, f.getFontData()[0].getName(), 26, SWT.BOLD);
		button.setFont(nf);
	}

	private SelectionListener launch2048() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IView ui = new Game2048View(display);
				IModel m = new Game2048Model(4);
				new Presenter(m, ui);
				shell.close();
				shell.dispose();
			}
		};

	}

	private SelectionListener launchMaze() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IView ui = new MazeView(display);
				IModel m = new MazeModel();
				new Presenter(m, ui);
				shell.close();
				shell.dispose();
			}
		};
	}
}
