package view;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

public class KeyboardHandler implements KeyListener {
	private static final long KEYBOARD_DELAY = 100;

	private View view;
	private boolean supportDiagonals;
	private ViewEvent lastEvent;
	private Timer timer;

	public KeyboardHandler(View view, boolean supportDiagonals) {
		this.view = view;
		this.supportDiagonals = supportDiagonals;
		if (supportDiagonals) {
			timer = new Timer();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		ViewEvent event = null;
		switch (e.keyCode) {
		case SWT.ARROW_UP:
			event = ViewEvent.UP;
			break;
		case SWT.ARROW_DOWN:
			event = ViewEvent.DOWN;
			break;
		case SWT.ARROW_LEFT:
			event = ViewEvent.LEFT;
			break;
		case SWT.ARROW_RIGHT:
			event = ViewEvent.RIGHT;
			break;
		default:
			return;
		}
		if (event != null)
			if (supportDiagonals) {
				if (lastEvent != null) {
					view.injectViewEvent(event.join(lastEvent));
					lastEvent = null;
				} else {
					lastEvent = event;
					startTimer();
				}
			} else {
				view.injectViewEvent(event);
			}
	}

	private void startTimer() {
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (lastEvent != null) {
					view.injectViewEvent(lastEvent);
					lastEvent = null;
				}
			}
		}, KEYBOARD_DELAY);
	}
}
