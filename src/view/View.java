package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import view.dialogs.InputDialog;
import view.dialogs.ServerInputDialog;

import common.SmartObservable;

public abstract class View extends SmartObservable implements SelectionListener, IView {

	private Display display;
	private Shell shell;
	private Board board;
	private Button buttonUndo;
	private Button buttonRestart;
	private Button buttonLoadGame;
	private Button buttonSave;
	private Label labelScore;

	private MenuItem menuItemExit;
	private MenuItem menuItemUndo;
	private MenuItem menuItemLoad;
	private MenuItem menuItemSave;
	private MenuItem menuItemRestart;

	private boolean alreadyWon;

	private Button buttonHelp1;
	private Button buttonHelpCustom;
	private Button buttonSetServer;

	public View(Display display) {
		this.display = display;
		init();
	}

	protected abstract Board getBoard(Composite parent);

	protected abstract boolean supportsDiagonals();

	protected abstract String getTitle();

	protected abstract boolean supportHelp();

	protected void onHelpRequested(int hints) {
	}

	private void init() {
		shell = new Shell(display);
		shell.setSize(1024, 768);
		shell.setLayout(new GridLayout(2, false));
		shell.setText(getTitle());

		shell.setFocus();
		initMenus();

		labelScore = new Label(shell, SWT.NONE);
		labelScore.setLayoutData(new GridData(SWT.CENTER, SWT.NONE, false, false, 2, 1));
		FontData[] fd = labelScore.getFont().getFontData();
		fd[0].setHeight(20);
		fd[0].setStyle(SWT.BOLD);
		labelScore.setFont(new Font(display, fd[0]));
		labelScore.setText("Score: 0");

		buttonUndo = new Button(shell, SWT.PUSH);
		buttonUndo.setText("Undo Move");
		buttonUndo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		buttonUndo.addSelectionListener(this);

		board = getBoard(shell);
		board.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 20));
		MouseHandler mouseHandler = new MouseHandler(this, supportsDiagonals());
		board.addMouseListener(mouseHandler);
		board.addMouseMoveListener(mouseHandler);
		board.addDragDetectListener(mouseHandler);

		buttonRestart = new Button(shell, SWT.PUSH);
		buttonRestart.setText("Restart Game");
		buttonRestart.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		buttonRestart.addSelectionListener(this);

		buttonLoadGame = new Button(shell, SWT.PUSH);
		buttonLoadGame.setText("Load Game");
		buttonLoadGame.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		buttonLoadGame.addSelectionListener(this);

		buttonSave = new Button(shell, SWT.PUSH);
		buttonSave.setText("Save Game");
		buttonSave.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		buttonSave.addSelectionListener(this);

		if (supportHelp()) {
			Label separator = new Label(shell, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

			buttonSetServer = new Button(shell, SWT.PUSH);
			buttonSetServer.setText("Set Server...");
			buttonSetServer.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
			buttonSetServer.addSelectionListener(this);

			buttonHelp1 = new Button(shell, SWT.PUSH);
			buttonHelp1.setText("Help! (1)");
			buttonHelp1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
			buttonHelp1.addSelectionListener(this);

			buttonHelpCustom = new Button(shell, SWT.PUSH);
			buttonHelpCustom.setText("Help...");
			buttonHelpCustom.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
			buttonHelpCustom.addSelectionListener(this);

		}

		shell.addKeyListener(new KeyboardHandler(this, supportsDiagonals()));
		shell.open();

		notifyChanges(ViewEvent.VIEW_READY);
	}

	private void initMenus() {
		Menu menuBar = new Menu(shell, SWT.BAR);

		{
			Menu fileMenu = new Menu(menuBar);

			MenuItem fileItem = new MenuItem(menuBar, SWT.CASCADE);
			fileItem.setText("File");
			fileItem.setMenu(fileMenu);

			menuItemRestart = new MenuItem(fileMenu, SWT.NONE);
			menuItemRestart.setText("Restart");
			menuItemRestart.addSelectionListener(this);

			menuItemSave = new MenuItem(fileMenu, SWT.NONE);
			menuItemSave.setText("Save");
			menuItemSave.addSelectionListener(this);

			menuItemLoad = new MenuItem(fileMenu, SWT.NONE);
			menuItemLoad.setText("Load");
			menuItemLoad.addSelectionListener(this);

			new MenuItem(fileMenu, SWT.SEPARATOR);

			menuItemExit = new MenuItem(fileMenu, SWT.NONE);
			menuItemExit.setText("Exit");
			menuItemExit.addSelectionListener(this);
		}

		{
			Menu editMenu = new Menu(menuBar);

			MenuItem editItem = new MenuItem(menuBar, SWT.CASCADE);
			editItem.setText("Edit");
			editItem.setMenu(editMenu);

			menuItemUndo = new MenuItem(editMenu, SWT.NONE);
			menuItemUndo.setText("Undo");
			menuItemUndo.addSelectionListener(this);
		}

		shell.setMenuBar(menuBar);
	}

	@Override
	public void displayData(final int[][] data, final int score, final boolean won, final boolean over) {
		if (board != null) {
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					board.setData(data);
					labelScore.setText("Score: " + score);
					if (won && !alreadyWon) {
						alreadyWon = true;
						win();
					} else if (over) {
						gameOver(score);
					}

					shell.layout();
				}
			});
		}
	}

	private void win() {
		MessageBox mbox = new MessageBox(shell, SWT.YES | SWT.NO);
		mbox.setText("You won the game!");
		mbox.setMessage("Keep Playing?");
		switch (mbox.open()) {
			case SWT.YES:
				break;
			case SWT.NO:
				resetGame();
				break;
		}
	}

	private void resetGame() {
		alreadyWon = false;
		buttonUndo.setEnabled(true);
		notifyChanges(ViewEvent.RESET);
	}

	private void gameOver(int score) {

		buttonUndo.setEnabled(false);

		MessageBox mbox = new MessageBox(shell);
		mbox.setText("Game Over!");
		mbox.setMessage("Game over. score: " + score);
		mbox.open();
	}

	public void injectViewEvent(ViewEvent event) {
		notifyChanges(event);
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		shell.forceFocus();
		if (e.widget instanceof Button) {
			buttonClick(e);
		} else
			menuItemClick(e);

	}

	private void menuItemClick(SelectionEvent e) {
		if (e.widget == menuItemExit) {
			shell.getDisplay().dispose();
			System.exit(0);
		} else if (e.widget == menuItemUndo) {
			notifyChanges(ViewEvent.UNDO);
		} else if (e.widget == menuItemLoad) {
			openLoadDialog();
		} else if (e.widget == menuItemSave) {
			openSaveDialog();
		} else if (e.widget == menuItemRestart) {
			resetGame();
		}
	}

	private void buttonClick(SelectionEvent e) {
		if (e.widget == buttonUndo) {
			notifyChanges(ViewEvent.UNDO);
		} else if (e.widget == buttonRestart) {
			resetGame();
		} else if (e.widget == buttonLoadGame) {
			openLoadDialog();
		} else if (e.widget == buttonSave) {
			openSaveDialog();
		} else if (e.widget == buttonHelp1) {
			onHelpRequested(1);
		} else if (e.widget == buttonHelpCustom) {
			InputDialog dialog = new InputDialog(shell);
			dialog.setText("Get Help!");
			dialog.setMessage("How many hints would you like to get?");
			String result = dialog.open();
			try {
				int hints = Integer.parseInt(result);
				onHelpRequested(hints);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (e.widget == buttonSetServer) {
			new ServerInputDialog(shell).open();
		}
	}

	private void openSaveDialog() {
		FileDialog fileDialog = new FileDialog(shell, SWT.SAVE);
		fileDialog.setOverwrite(true);
		String path = fileDialog.open();
		if (path != null)
			notifyChanges(ViewEvent.SAVE.setArgs(path));
	}

	private void openLoadDialog() {
		String path = new FileDialog(shell, SWT.OPEN).open();
		if (path != null)
			notifyChanges(ViewEvent.LOAD.setArgs(path));
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {

	}
}
