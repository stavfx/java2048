package view;

import java.awt.Point;

import org.eclipse.swt.events.DragDetectEvent;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;

public class MouseHandler implements MouseListener, MouseMoveListener, DragDetectListener {
	private static final int FIRST_RADIUS = 50;
	private static final int SECOND_RADIUS = 20;

	private View view;

	private int startX;
	private int startY;

	private boolean isDragging;

	private final boolean supportDiagonals;

	public MouseHandler(View view, boolean supportDiagonals) {
		this.view = view;
		this.supportDiagonals = supportDiagonals;
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {

	}

	@Override
	public void mouseDown(MouseEvent event) {
		startX = event.x;
		startY = event.y;
	}

	@Override
	public void mouseUp(MouseEvent event) {
		isDragging = false;
	}

	@Override
	public void dragDetected(DragDetectEvent event) {
		isDragging = true;
	}

	@Override
	public void mouseMove(MouseEvent event) {
		if (!isDragging)
			return;

		Point direction = new Point();
		int dx = event.x - startX;
		if (Math.abs(dx) > FIRST_RADIUS) {
			direction.x = dx > 0 ? 1 : -1;
			isDragging = false;
		}

		int dy = event.y - startY;
		if (Math.abs(dy) > FIRST_RADIUS) {
			direction.y = dy > 0 ? 1 : -1;
			isDragging = false;
		}

		if (supportDiagonals) {
			// second check for better diagonal detection
			if (direction.x != 0 && Math.abs(dy) > SECOND_RADIUS)
				direction.y = dy > 0 ? 1 : -1;
			if (direction.y != 0 && Math.abs(dx) > SECOND_RADIUS)
				direction.x = dx > 0 ? 1 : -1;

			if (direction.x == 0 && direction.y < 0)
				view.injectViewEvent(ViewEvent.UP);
			else if (direction.x > 0 && direction.y < 0)
				view.injectViewEvent(ViewEvent.UP_RIGHT);
			else if (direction.x > 0 && direction.y == 0)
				view.injectViewEvent(ViewEvent.RIGHT);
			else if (direction.x > 0 && direction.y > 0)
				view.injectViewEvent(ViewEvent.DOWN_RIGHT);
			else if (direction.x == 0 && direction.y > 0)
				view.injectViewEvent(ViewEvent.DOWN);
			else if (direction.x < 0 && direction.y > 0)
				view.injectViewEvent(ViewEvent.DOWN_LEFT);
			else if (direction.x < 0 && direction.y == 0)
				view.injectViewEvent(ViewEvent.LEFT);
			else if (direction.x < 0 && direction.y < 0)
				view.injectViewEvent(ViewEvent.UP_LEFT);
		} else {

			if (direction.x > 0)
				view.injectViewEvent(ViewEvent.RIGHT);
			else if (direction.x < 0)
				view.injectViewEvent(ViewEvent.LEFT);
			else if (direction.y > 0)
				view.injectViewEvent(ViewEvent.DOWN);
			else if (direction.y < 0)
				view.injectViewEvent(ViewEvent.UP);

		}
	}
}
