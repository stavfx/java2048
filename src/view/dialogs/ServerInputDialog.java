package view.dialogs;

import http.ConfigHandler;
import http.ConfigHandler.ServerConfig;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Shell;

public class ServerInputDialog extends InputDialog {
	private ServerConfig servers;

	public ServerInputDialog(Shell parent) {
		super(parent);
		servers = ConfigHandler.getServerConfig();
		setText("Select Server...");
	}

	@Override
	protected void createContents(final Shell shell) {
		shell.setLayout(new GridLayout(2, true));

		final Combo combo = new Combo(shell, SWT.DROP_DOWN);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		combo.setLayoutData(data);
		combo.setItems(servers.toArray());
		combo.setText(servers.getSelectedServer());

		Button ok = new Button(shell, SWT.PUSH);
		ok.setText("OK");
		data = new GridData(GridData.FILL_HORIZONTAL);
		ok.setLayoutData(data);
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				ConfigHandler.setCurrentServer(combo.getText());
				shell.close();
			}
		});

		Button cancel = new Button(shell, SWT.PUSH);
		cancel.setText("Cancel");
		data = new GridData(GridData.FILL_HORIZONTAL);
		cancel.setLayoutData(data);
		cancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				shell.close();
			}
		});

		shell.setDefaultButton(ok);
	}
}
