package view;

import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

public abstract class BaseTile extends Canvas {
	private int value;

	public BaseTile(Composite parent, int style) {
		super(parent, style);
	}

	public final void setValue(int value) {
		this.value = value;
		onValueChanged(value);
	}

	public int getValue() {
		return value;
	}

	protected void onValueChanged(int newValue) {

	}
}
