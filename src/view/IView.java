package view;

import java.util.Observer;

public interface IView {
	void addObserver(Observer o);

	void displayData(final int[][] data, final int score, final boolean won, final boolean over);
}
