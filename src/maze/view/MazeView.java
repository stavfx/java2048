package maze.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import view.Board;
import view.View;

public class MazeView extends View {
	public MazeView(Display display) {
		super(display);
	}

	@Override
	protected Board getBoard(Composite parent) {
		return new MazeBoard(parent, SWT.BORDER);
	}

	@Override
	protected boolean supportsDiagonals() {
		return true;
	}

	@Override
	protected String getTitle() {
		return "Maze Time!";
	}

	@Override
	protected boolean supportHelp() {
		return false;
	}
}
