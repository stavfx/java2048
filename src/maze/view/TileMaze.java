package maze.view;

import maze.model.MazeObjects;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import view.BaseTile;

public class TileMaze extends BaseTile implements PaintListener {
	private static Image mouseImage;
	private static Image cheeseImage;
	private static Image wallImage;

	public TileMaze(Composite parent, int style) {
		super(parent, style);
		initImages();
		setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		addPaintListener(this);
	}

	private void initImages() {
		if (mouseImage == null)
			mouseImage = new Image(getDisplay(), "assets/mouse.png");

		if (cheeseImage == null)
			cheeseImage = new Image(Display.getDefault(), "assets/cheese.png");

		if (wallImage == null)
			wallImage = new Image(Display.getDefault(), "assets/wall.jpg");
	}

	@Override
	public void paintControl(PaintEvent e) {
		switch (getValue()) {
		case MazeObjects.FREE_CELL: {
			// do nothing, just white
		}
			break;
		case MazeObjects.PLAYER: {
			e.gc.drawImage(new Image(Display.getDefault(), mouseImage.getImageData().scaledTo(e.width, e.height)), 0, 0);
		}
			break;
		case MazeObjects.WALL: {
			e.gc.drawImage(new Image(Display.getDefault(), wallImage.getImageData().scaledTo(e.width, e.height)), 0, 0);
		}
			break;
		case MazeObjects.FINISH: {
			e.gc.drawImage(new Image(Display.getDefault(), cheeseImage.getImageData().scaledTo(e.width, e.height)), 0, 0);
		}
			break;
		default:
			setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE));
			break;
		}
	}

	@Override
	protected void onValueChanged(int newValue) {
		redraw();
	}

}
