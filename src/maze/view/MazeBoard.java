package maze.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import view.BaseTile;
import view.Board;

public class MazeBoard extends Board {

	public MazeBoard(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected BaseTile makeNewTile() {
		return new TileMaze(this, SWT.NONE);
	}

}
