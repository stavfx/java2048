package maze.graph_solver;

import java.util.ArrayList;
import java.util.HashMap;

import maze.model.MazeModel;

public class Astar extends AbsSearcher {

	private Domain domain;
	private Distance g, h;

	public Astar() {
		this.domain = new MazeDomain(new MazeModel());
		this.g = new MazeDistanceG();
		this.h = new MazeAirDistance();
	}

	public Astar(Domain domain, Distance g, Distance h) {

		this.domain = domain;
		this.g = g;
		this.h = h;
	}

	@Override
	public double search() {
		Node sstart = domain.getStartNode();
		Node sgoal = domain.getGoalNode();
		Node father, current;
		Node start = getNode(sstart);
		Node goal = getNode(sgoal);
		double tempGScore = 0;
		ArrayList<MazeAction> legalActions;
		HashMap<Node, NodeMazeActionPair> cameFrom = new HashMap<Node, NodeMazeActionPair>();

		addToOpenList(start);
		start.setG(g.getDistance(start, start));
		start.setF(g.getDistance(start, start) + h.getDistance(start, goal));

		while (!isOpenListEmpty())
		{
			father = pollFromOpenList();
			if (father.equals(goal)) {
				return reconstructPath(cameFrom, father);
			}
			addToClosedList(father);
			legalActions = domain.getMazeActions(father);
			for (MazeAction a : legalActions)
			{
				current = getNode(a.doAction(father));
				tempGScore = father.getG() + g.getDistance(father, current);

				if (closedListContains(current) && tempGScore >= current.getG())
					continue;
				if (!openListContains(current) || tempGScore < current.getG())
				{
					cameFrom.put(current, new NodeMazeActionPair(father, a));
					current.setG(tempGScore);
					current.setF(current.getG() + h.getDistance(current, goal));
					if (!openListContains(current))
						addToOpenList(current);
				}
			}

		}

		return 0;

	}

	private double reconstructPath(HashMap<Node, NodeMazeActionPair> cameFrom, Node current)
	{
		double score = 0;
		if (cameFrom == null || cameFrom.isEmpty())
			return 0;
		while (cameFrom.containsKey(current) && cameFrom.get(current).getNode() != null) {
			score += g.getDistance(current, cameFrom.get(current).getNode());
			current = cameFrom.get(current).getNode();
		}
		return score;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Distance getG() {
		return g;
	}

	public void setG(Distance g) {
		this.g = g;
	}

	public Distance getH() {
		return h;
	}

	public void setH(Distance h) {
		this.h = h;
	}
}
