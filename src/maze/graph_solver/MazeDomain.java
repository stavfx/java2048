package maze.graph_solver;

import java.util.ArrayList;

import maze.model.MazeModel;
import maze.model.MazeObjects;

import org.eclipse.swt.graphics.Point;

public class MazeDomain implements Domain {

	private MazeModel maze;

	public MazeDomain() {
		this.maze = new MazeModel();
	}

	public MazeDomain(MazeModel maze) {
		this.maze = maze;
	}

	@Override
	public ArrayList<MazeAction> getMazeActions(Node node) {
		Point point = (Point) node.getNode();
		ArrayList<MazeAction> actions = new ArrayList<MazeAction>();

		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (i == 0 && j == 0)
					continue;
				int x = point.x + i;
				int y = point.y + j;
				if (maze.getCell(x, y) != MazeObjects.WALL && maze.getCell(x, y) != -1)
					actions.add(new MazeAction(i, j));
			}
		}
		return actions;

	}

	@Override
	public Node getStartNode() {
		return new Node(maze.getStart());
	}

	@Override
	public Node getGoalNode() {
		return new Node(maze.getFinish());
	}

	public MazeModel getMaze() {
		return maze;
	}

	public void setMaze(MazeModel maze) {
		this.maze = maze;
	}

}
