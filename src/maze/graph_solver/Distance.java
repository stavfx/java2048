package maze.graph_solver;

public interface Distance {

	public double getDistance(Node from, Node to);

}
