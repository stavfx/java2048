package maze.graph_solver;

import org.eclipse.swt.graphics.Point;

public class MazeAirDistance implements Distance {

	@Override
	public double getDistance(Node from, Node to) {
		Point pFrom = (Point) from.getNode();
		Point pTo = (Point) to.getNode();

		int rows = Math.abs(pTo.x - pFrom.x);
		int columns = Math.abs(pTo.y - pFrom.y);

		return (Math.sqrt((rows * rows) + (columns * columns)) * 10);
	}

}
