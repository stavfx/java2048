package maze.model;

public final class MazeObjects {
	private MazeObjects() {
	}

	public static final int FREE_CELL = 1;
	public static final int WALL = 0;
	public static final int FINISH = 3;
	public static final int PLAYER = 8;
}
