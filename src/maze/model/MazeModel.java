package maze.model;

import maze.graph_solver.Astar;
import maze.graph_solver.MazeAirDistance;
import maze.graph_solver.MazeDistanceG;
import maze.graph_solver.MazeDomain;
import model.GameState;
import model.Model;

import org.eclipse.swt.graphics.Point;

public class MazeModel extends Model {
	private static final int[][] START_MAZE = {
			{ 3, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1 },
			{ 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1 },
			{ 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1 },
			{ 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 1, 1 }
	};

	private int playerX;
	private int playerY;

	private int startX;
	private int startY;

	private int finishX;
	private int finishY;
	private int minimalScore;

	public MazeModel() {
		reset();
	}

	@Override
	public void moveUp() {
		move(0, -1);
	}

	@Override
	public void moveDown() {
		move(0, 1);
	}

	@Override
	public void moveLeft() {
		move(-1, 0);
	}

	@Override
	public void moveRight() {
		move(1, 0);
	}

	@Override
	public void moveUpRight() {
		move(1, -1);
	}

	@Override
	public void moveDownLeft() {
		move(-1, 1);
	}

	@Override
	public void moveUpLeft() {
		move(-1, -1);
	}

	@Override
	public void moveDownRight() {
		move(1, 1);
	}

	@Override
	public void reset() {
		int[][] data = new int[START_MAZE.length][];
		for (int i = 0; i < START_MAZE.length; i++) {
			data[i] = START_MAZE[i].clone();
		}

		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				if (data[i][j] == MazeObjects.PLAYER) {
					startX = playerX = j;
					startY = playerY = i;
				} else if (data[i][j] == MazeObjects.FINISH) {
					finishX = j;
					finishY = i;
				}
			}
		}
		Astar as = new Astar(new MazeDomain(this), new MazeDistanceG(), new MazeAirDistance());
		minimalScore = (int) as.search();
		// System.out.println("min=" + minimalScore);

		initData(data);
		publishCurrentState();
	}

	public Point getStart() {
		return new Point(startX, startY);
	}

	public Point getFinish() {
		return new Point(finishX, finishY);
	}

	private void move(int dx, int dy) {
		if (getGameState() == GameState.STARTED) {
			int newX = playerX + dx;
			int newY = playerY + dy;
			if (isValidMove(newX, newY)) {
				saveState(new Point(playerX, playerY));
				addScore(getMoveCost(dx, dy));
				checkWin(newX, newY);
				setCell(playerX, playerY, MazeObjects.FREE_CELL);
				setCell(newX, newY, MazeObjects.PLAYER);
				playerX = newX;
				playerY = newY;

				publishCurrentState();
			}
		}
	}

	@Override
	protected void onUndo(Object args) {
		if (args instanceof Point) {
			Point p = (Point) args;
			playerX = p.x;
			playerY = p.y;
		}
	}

	private void checkWin(int newX, int newY) {
		if (getCell(newX, newY) == MazeObjects.FINISH) {
			if (isMinimalScore())
				setGameState(GameState.WON);
			else
				setGameState(GameState.OVER);
		}
	}

	private boolean isMinimalScore() {
		return minimalScore == getScore();
	}

	private int getMoveCost(int dx, int dy) {
		if (dx != 0 && dy != 0)
			return 15;
		return 10;
	}

	private boolean isValidMove(int newX, int newY) {
		int cell = getCell(newX, newY);
		return cell == MazeObjects.FREE_CELL || cell == MazeObjects.FINISH;
	}
}
