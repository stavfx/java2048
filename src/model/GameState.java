package model;

public enum GameState {
	STARTED,
	OVER,
	WON
}
