package model;

import java.util.Observer;

public interface IModel {
	void addObserver(Observer o);

	void setCell(int x, int y, int value);

	GameState getGameState();

	int[][] getData();

	int getScore();

	void moveDown();

	void moveLeft();

	void moveRight();

	void moveUp();

	void moveDownLeft();

	void moveDownRight();

	void moveUpLeft();

	void moveUpRight();

	void undo();

	void reset();

	void saveToFile(String path);

	void loadFromFile(String path);
}
