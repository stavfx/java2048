package model;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.SmartObservable;

public abstract class Model extends SmartObservable implements IModel {
	private int[][] data;
	private int score;
	private GameState gameState;

	private Stack<ModelState> history = new Stack<ModelState>();

	@Override
	public void setCell(int x, int y, int value) {
		if (isInBounds(x, y)) {
			data[y][x] = value;
		}
	}

	public void setCellIfEmpty(int x, int y, int value) {
		if (getCell(x, y) == 0)
			setCell(x, y, value);
	}

	protected void saveState() {
		saveState(null);
	}

	protected void saveState(Object args) {
		history.push(new ModelState(data, score, gameState, args));
	}

	protected void deleteLastState() {
		if (history.size() > 0)
			history.pop();
	}

	@Override
	public void saveToFile(String path) {
		File file = new File(path);
		if (!file.exists() || file.isFile()) {
			try {
				saveState();
				String json = new GsonBuilder().create().toJson(history);
				Files.write(file.toPath(), json.getBytes());
				undo();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void loadFromFile(String path) {
		File file = new File(path);
		if (file.exists() && file.isFile()) {
			try {
				byte[] bytes = Files.readAllBytes(file.toPath());
				String json = new String(bytes);
				ModelState[] history = new Gson().fromJson(json, ModelState[].class);
				List<ModelState> list = Arrays.asList(history);
				this.history.clear();
				this.history.addAll(list);
				undo();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void addScore(int value) {
		score += value;
	}

	public int getCell(int x, int y) {
		if (isInBounds(x, y))
			return data[y][x];
		return -1;
	}

	protected boolean isInBounds(int x, int y) {
		if (data == null)
			return false;

		if (x < 0 || y < 0)
			return false;

		if (data.length <= y || data[y].length <= x)
			return false;

		return true;
	}

	@Override
	public int[][] getData() {
		return data;
	}

	public void setData(int[][] data) {
		this.data = data;
		publishCurrentState();
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState state) {
		this.gameState = state;
	}

	protected void initData(int[][] newData) {
		history.clear();
		this.score = 0;
		this.data = newData;
		this.gameState = GameState.STARTED;
	}

	@Override
	public void undo() {
		if (history.size() > 0) {
			ModelState state = history.pop();
			score = state.getScore();
			data = state.getData();
			gameState = state.getState();
			onUndo(state.getArgs());
			publishCurrentState();
		}
	}

	protected void onUndo(Object args) {
	}

	protected void publishCurrentState() {
		notifyChanges(new ModelState(getData(), getScore(), getGameState()));
	}

	@Deprecated
	public void print() {
		if (data == null) {
			System.out.println("Model data is null");
			return;
		}

		for (int y = 0; y < data.length; y++) {
			if (data[y] == null) {
				System.out.println("null");
				continue;
			}

			for (int x = 0; x < data[y].length; x++) {
				System.out.print(getCell(x, y) + ", ");
			}
			System.out.println();
		}
	}
}
