package model;

public class ModelState {
	private int[][] data;
	private int score;
	private GameState state;
	private Object args;

	public ModelState(int[][] _data, int score, GameState state) {
		this(_data, score, state, null);
	}

	public ModelState(int[][] _data, int score, GameState state, Object args) {
		this.score = score;
		this.state = state;
		this.args = args;

		data = new int[_data.length][];
		for (int i = 0; i < _data.length; i++)
			data[i] = _data[i].clone();
	}

	public int[][] getData() {
		return data;
	}

	public int getScore() {
		return score;
	}

	public GameState getState() {
		return state;
	}

	public Object getArgs() {
		return args;
	}

}
