package game_2048.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import model.GameState;
import model.Model;

import org.eclipse.swt.graphics.Point;

public class Game2048Model extends Model {
	private static final float TWO_CHANCE = 0.9f;
	private static final int START_TILES_NUM = 2;

	private Random mRandom;
	private int size;

	public Game2048Model(int size) {
		mRandom = new Random();
		this.size = size;
		reset();
	}

	@Override
	public void reset() {
		initData(new int[size][size]);
		addStartTiles();

		// test win+lose scenario
		// initData(new int[][] {
		// { 1024, 1024, 1, 3 },
		// { 7, 9, 11, 13 },
		// { 15, 17, 19, 21 },
		// { 23, 25, 27, 29 }
		// });
	}

	@Override
	public void moveUp() {
		slideAndMerge(Direction.UP);
	}

	@Override
	public void moveDown() {
		slideAndMerge(Direction.DOWN);
	}

	@Override
	public void moveLeft() {
		slideAndMerge(Direction.LEFT);
	}

	@Override
	public void moveRight() {
		slideAndMerge(Direction.RIGHT);
	}

	private void addStartTiles() {
		for (int i = 0; i < START_TILES_NUM; i++) {
			addRandomTile();
		}
		publishCurrentState();
	}

	private void addRandomTile() {
		int number = getNextNumber();
		Point position = getRandomFreeCell();

		setCell(position.x, position.y, number);
	}

	private Point getRandomFreeCell() {
		ArrayList<Point> freeCells = new ArrayList<Point>();
		for (int y = 0; y < getData().length; y++) {
			for (int x = 0; x < getData()[y].length; x++) {
				if (getCell(x, y) == 0)
					freeCells.add(new Point(x, y));
			}
		}

		if (freeCells.size() == 0)
			return null;

		int position = mRandom.nextInt(freeCells.size());
		return freeCells.get(position);
	}

	private int getNextNumber() {
		if (mRandom.nextFloat() < TWO_CHANCE)
			return 2;
		return 4;
	}

	private void slideAndMerge(Direction direction) {
		if (getGameState() == GameState.OVER)
			return;
		saveState();
		HashSet<Point> merged_cells = new HashSet<Point>();
		Traversals traversals = getTraversals(direction);
		boolean somethingMoved = false;
		for (Integer x : traversals.x) {
			for (Integer y : traversals.y) {
				int current = getCell(x, y);
				if (current == 0)
					continue;
				Point start = new Point(x, y);
				Point farthest = findFarthestPosition(start, direction);

				Point nextPoint = new Point(farthest.x + direction.dx, farthest.y + direction.dy);
				int next_value = getCell(nextPoint.x, nextPoint.y);
				boolean already_merged = merged_cells.contains(nextPoint);
				if (!already_merged && current == next_value) {
					setCell(x, y, 0);
					somethingMoved = true;
					merged_cells.add(nextPoint);
					int newValue = next_value * 2;
					addScore(newValue);
					setCell(nextPoint.x, nextPoint.y, newValue);
					if (newValue == 2048) {
						setGameState(GameState.WON);
						publishCurrentState();
					}
				} else if (!start.equals(farthest)) {
					setCell(x, y, 0);
					somethingMoved = true;
					setCell(farthest.x, farthest.y, current);
				}
			}
		}

		if (somethingMoved) {
			addRandomTile();
			checkGameOver();
			publishCurrentState();
		} else
			deleteLastState();
	}

	private void checkGameOver() {
		if (!hasFreeCells() && !isMergePossible()) {
			setGameState(GameState.OVER);
		}
	}

	private boolean isMergePossible() {
		for (int x = 0; x < getData().length; x++) {
			for (int y = 0; y < getData()[x].length; y++) {
				for (Direction direction : Direction.values())
					if (getCell(x, y) == getCell(x + direction.dx, y + direction.dy))
						return true;
			}
		}
		return false;
	}

	private boolean hasFreeCells() {
		for (int i = 0; i < getData().length; i++) {
			for (int j = 0; j < getData()[i].length; j++) {
				if (getCell(i, j) == 0)
					return true;
			}
		}
		return false;
	}

	private Traversals getTraversals(Direction direction) {
		Traversals traversals = new Traversals();
		for (int i = 0; i < getData().length; i++) {
			traversals.x.add(i);
			traversals.y.add(i);
		}

		if (direction.dx == 1)
			Collections.reverse(traversals.x);
		if (direction.dy == 1)
			Collections.reverse(traversals.y);
		return traversals;
	}

	private Point findFarthestPosition(Point from, Direction direction) {
		Point previous;
		do {
			previous = from;
			from = new Point(previous.x + direction.dx, previous.y + direction.dy);
		} while (isInBounds(from.x, from.y) && getCell(from.x, from.y) == 0);

		return previous;
	}

	private static class Traversals {
		List<Integer> x = new ArrayList<Integer>();
		List<Integer> y = new ArrayList<Integer>();
	}

	private enum Direction {
		UP(0, -1),
		DOWN(0, 1),
		LEFT(-1, 0),
		RIGHT(1, 0);

		int dx;
		int dy;

		private Direction(int dx, int dy) {
			this.dx = dx;
			this.dy = dy;
		}
	}

	@Override
	public void moveUpRight() {
		throw new UnsupportedOperationException("This model can't move diagonally!");
	}

	@Override
	public void moveDownLeft() {
		throw new UnsupportedOperationException("This model can't move diagonally!");
	}

	@Override
	public void moveUpLeft() {
		throw new UnsupportedOperationException("This model can't move diagonally!");
	}

	@Override
	public void moveDownRight() {
		throw new UnsupportedOperationException("This model can't move diagonally!");
	}
}
