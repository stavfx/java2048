package game_2048.view;

import http.ConfigHandler;
import http.HelpRequest;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import view.Board;
import view.View;

public class Game2048View extends View {

	public Game2048View(Display display) {
		super(display);
	}

	@Override
	protected Board getBoard(Composite parent) {
		return new Game2048Board(parent, SWT.BORDER);
	}

	@Override
	protected boolean supportsDiagonals() {
		return false;
	}

	@Override
	protected String getTitle() {
		return "2048!";
	}

	@Override
	protected boolean supportHelp() {
		return true;
	}

	@Override
	protected void onHelpRequested(int hints) {
		String server = ConfigHandler.getCurrentServer();
		notifyChanges(new HelpRequest("http://" + server + "/2048", hints));
	}

}
