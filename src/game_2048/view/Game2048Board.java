package game_2048.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

import view.BaseTile;
import view.Board;

public class Game2048Board extends Board {

	public Game2048Board(Composite parent, int style) {
		super(parent, style);
		setBackground(new Color(getDisplay(), 187, 173, 160));
	}

	@Override
	protected BaseTile makeNewTile() {
		return new Tile2048(this, SWT.NONE);
	}
}
