package game_2048.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.widgets.Composite;

import view.BaseTile;

public class Tile2048 extends BaseTile implements PaintListener {

	public Tile2048(Composite parent, int style) {
		super(parent, style);

		Font f = getFont();
		Font nf = new Font(getDisplay(), f.getFontData()[0].getName(), 16, SWT.BOLD);
		setFont(nf);
		addPaintListener(this);
	}

	@Override
	protected void onValueChanged(int newValue) {
		changeBackgroundColor();
		redraw();

	}

	private void changeBackgroundColor() {
		switch (getValue()) {
		case 0:
			setBackground(new Color(getDisplay(), 204, 192, 179));
			break;
		case 2:
			setBackground(new Color(getDisplay(), 238, 228, 218));
			setForeground(new Color(getDisplay(), 119, 100, 101));
			break;
		case 4:
			setBackground(new Color(getDisplay(), 237, 224, 200));
			setForeground(new Color(getDisplay(), 119, 100, 101));
			break;
		case 8:
			setBackground(new Color(getDisplay(), 242, 177, 121));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 16:
			setBackground(new Color(getDisplay(), 245, 149, 99));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 32:
			setBackground(new Color(getDisplay(), 246, 124, 95));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 64:
			setBackground(new Color(getDisplay(), 246, 94, 59));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 128:
			setBackground(new Color(getDisplay(), 237, 207, 114));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 256:
			setBackground(new Color(getDisplay(), 237, 204, 97));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 512:
			setBackground(new Color(getDisplay(), 237, 200, 80));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 1024:
			setBackground(new Color(getDisplay(), 237, 197, 63));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 2048:
			setBackground(new Color(getDisplay(), 237, 194, 46));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		case 4096:
			setBackground(new Color(getDisplay(), 60, 58, 50));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		default:
			setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_MAGENTA));
			setForeground(new Color(getDisplay(), 249, 246, 242));
			break;
		}
	}

	@Override
	public void paintControl(PaintEvent e) {
		int value = getValue();
		FontMetrics fm = e.gc.getFontMetrics();
		int width = fm.getAverageCharWidth();
		int mx = getSize().x / 2 - ("" + value).length() * width / 2;
		int my = getSize().y / 2 - fm.getHeight() / 2 - fm.getDescent();
		if (value > 0)
			e.gc.drawString("" + value, mx, my);
	}

}
